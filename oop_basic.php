<?php


class person{
    public $name;  //property
    public $age;   //property
    public function __construct() //magic method
    {
        echo "i m inside the construct method . <br>";
    }
    public function __destruct() //magic method
    {
        echo "maf kore diyen.<br>";
    }


    public function setName($name){ //method
        $this->name = $name;
    }
    public function setage($age){    //method

    $this->age = $age;
}
}

$objperson = new person();     //obj
$objperson ->setname("jorina"); //encapsulation
$objbekti = new person ();     //obj
$objbekti  -> setname("kalam"); //encapsulation
echo $objperson -> name."<br>";

class student extends person{

    public function dosomething(){
        echo $this->name ."<br>";
    }
}

    //inheritence

$objstudent = new student (); //obj
$objstudent -> setname("rahim"); //encapsulation
echo $objstudent-> name ."<br>"; //encapsulation

?>